package com.example.anton;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.example.anton.nieuw.R;

import java.util.ArrayList;

/**
 * Created by Jun-Wei on 2-12-2015.
 */
public class PlayersAdapter extends ArrayAdapter<Players> {

    ArrayList<Players> ArrayListPlayers;
    int Resource;
    Context context;
    LayoutInflater vi;

    public PlayersAdapter(Context context, int resource, ArrayList<Players> objects) {
        super(context, resource, objects);

        ArrayListPlayers = objects;
        Resource = resource;
        this.context = context;

        vi = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(Resource, null);
            holder = new ViewHolder();

            holder.tvPlayer = (TextView)convertView.findViewById(R.id.tvPlayer);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.tvPlayer.setText(ArrayListPlayers.get(position).getPlayer());
        return convertView;

    }

    static class ViewHolder {
        public TextView tvPlayer;
    }
}
