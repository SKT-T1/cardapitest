package com.example.anton.nieuw;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.anton.gameplay.CallAPI;
import com.example.anton.gameplay.HashMapBuilder;
import com.example.anton.gameplay.OnTaskCompleted;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class StartGameActivity extends Cards implements OnTaskCompleted {

    private final OnTaskCompleted onTaskCompleted = this;//enables call-back methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startgame);
        theDeck1 = (ImageView) findViewById(R.id.deck1);
        theDeck2 = (ImageView) findViewById(R.id.deck2);
        theDeck3 = (ImageView) findViewById(R.id.deck3);
        myCard1 = (ImageView) findViewById(R.id.card1);
        myCard2 = (ImageView) findViewById(R.id.card2);
        myCard3 = (ImageView) findViewById(R.id.card3);
        final int notSelected = getResources().getIdentifier("cardborder", "drawable", getPackageName());
        final int selected = getResources().getIdentifier("cardborderclick", "drawable", getPackageName());
        //button_swap_cards
        final Button swap_cardsbutton = (Button) findViewById(R.id.button_swap_cards);
        swap_cardsbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                name = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Gebruikersnaam), "");
                gamenr = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.game_number), "");
                String urlString = CARD_API + String.format("games/%s/%s/play/swap", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.get_deck)).execute(urlString, getString(R.string.POST),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                theDeck1.setBackgroundResource(notSelected);
                theDeck2.setBackgroundResource(notSelected);
                theDeck3.setBackgroundResource(notSelected);
                myCard1.setBackgroundResource(notSelected);
                myCard2.setBackgroundResource(notSelected);
                myCard3.setBackgroundResource(notSelected);
            }
        });

        //button_swapcard
        final Button swapcardbutton = (Button) findViewById(R.id.button_swap_card);
        swapcardbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                name = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Gebruikersnaam), "");
                gamenr = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.game_number), "");
                String cardone = ((EditText) findViewById(R.id.id_cardOne)).getText().toString();//from deck
                String cardtwo = ((EditText) findViewById(R.id.id_cardTwo)).getText().toString();//from hand
                if (cardone.equals("") || cardtwo.equals("")) {
                    Toast.makeText(getApplicationContext(), "Pick a card from the deck and a card from your hand.\n", Toast.LENGTH_LONG).show();
                } else {
                    String urlString = CARD_API + String.format("games/%s/%s/play/move/%s/%s", name, gamenr, cardone, cardtwo);
                    new CallAPI(onTaskCompleted, getString(R.string.swap_card)).execute(urlString, getString(R.string.POST), CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                    ((EditText) findViewById(R.id.id_cardOne)).setText("");
                    ((EditText) findViewById(R.id.id_cardTwo)).setText("");
                    theDeck1.setBackgroundResource(notSelected);
                    theDeck2.setBackgroundResource(notSelected);
                    theDeck3.setBackgroundResource(notSelected);
                    myCard1.setBackgroundResource(notSelected);
                    myCard2.setBackgroundResource(notSelected);
                    myCard3.setBackgroundResource(notSelected);
                }
            }
        });
        //button_pass
        final Button passbutton = (Button) findViewById(R.id.button_pass);
        passbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                name = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Gebruikersnaam), "");
                gamenr = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.game_number), "");
                String urlString = CARD_API + String.format("games/%s/%s/play/pass", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.pass)).execute(urlString, getString(R.string.POST),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                theDeck1.setBackgroundResource(notSelected);
                theDeck2.setBackgroundResource(notSelected);
                theDeck3.setBackgroundResource(notSelected);
                myCard1.setBackgroundResource(notSelected);
                myCard2.setBackgroundResource(notSelected);
                myCard3.setBackgroundResource(notSelected);
            }
        });


        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                // task to be done every 1500 milliseconds
                name = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Gebruikersnaam), "");
                password = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Wachtwoord), "");
                gamenr = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.game_number), "");
                String urlString;
                urlString = CARD_API + String.format("games/%s/%s/getcards", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.get_cards)).execute(urlString, getString(R.string.POST),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                urlString = CARD_API + String.format("games/%s/%s/deck", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.get_deck)).execute(urlString, getString(R.string.GET),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                urlString = CARD_API + String.format("games/%s/%s/state", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.game_status)).execute(urlString, getString(R.string.GET),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                urlString = CARD_API + String.format("games/%s/%s/players", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.listplayers)).execute(urlString, getString(R.string.GET));
                urlString = CARD_API + String.format("games/%s/%s/token", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.get_token)).execute(urlString, getString(R.string.GET));
            }
        }, 0, 1500);
    }

    /**
     * displays result of game status
     *
     * @param result = json-string
     */
    public void gameStatus(String result){
        try {
            JSONObject jsonObject = new JSONObject(result);
            String jsonGameStatusString = jsonObject.getString("status");
            if (jsonGameStatusString.equals("ended") && isInFront.equals("true")) {
                Toast.makeText(getApplicationContext(), "The game has ended.\n", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(StartGameActivity.this, WinnerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), "You cannot leave.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTaskCompleted(String result, String call) {
        if (call.equals(getString(R.string.get_cards))) {
            getCards(result);
        }
        if (call.equals(getString(R.string.get_deck))) {
            getDeck(result);
        }
        if (call.equals(getString(R.string.swap_cards))) {
            swapCards(result);
        }
        if (call.equals(getString(R.string.swap_card))) {
            swapCard(result);
        }
        if (call.equals(getString(R.string.pass))) {
            pass(result);
        }
        if (call.equals(getString(R.string.game_status))) {
            gameStatus(result);
        }
        if (call.equals(getString(R.string.get_token))) {
            gettoken(result);
        }
        if (call.equals(getString(R.string.listplayers))) {
            gamePlayers(result);
        }
    }
}
