![SKT1.png](https://bitbucket.org/repo/xqr4GB/images/2727838415-SKT1.png)  
SK Telecom T1 is a Korean a professional gaming team based in South Korea. Previously, the SK Telecom organization sponsored two sister teams, S and K.  
  
It was originally founded as a StarCraft: Brood War team under the name Orion in 2002 by Lim Yo-Hwan (Boxer), regarded by many as the greatest eSports player of all time, and has included such stars as Kim Taek-Young (Bisu), Jung Myung-Hoon (Fantasy) and Choi Yun-Sung (ILoveOov - who is currently the coach of SKT1's StarCraft II team). Its League and StarCraft teams have made efforts to support each other publicly, as cameras caught Faker and the rest of the League team attending one of the StarCraft team's Proleague matches against Jin Air

### Sponsors ###
[SK Telecom](http://www.sktelecom.com/index_real.html)  
[Pocari Sweat](http://www.donga-otsuka.co.kr/index.asp)  
[New balance](http://www.newbalance.com/international)  
[Corsair](http://www.corsair.com/en)  


## [Follow us on Twitter!](https://twitter.com/sktelecom_t1)