package com.example.anton.nieuw;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.anton.UserList;
import com.parse.ParseUser;

/**
 * Created by Jun-Wei on 5-1-2016.
 */
public class MenuMain extends GlobalVariables {

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater getMenuInflater = getMenuInflater();
        getMenuInflater.inflate(R.menu.menu_main, menu);
        mPlayMenuItem = menu.findItem(R.id.action_play);
        mStopMenuItem = menu.findItem(R.id.action_stop);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_play:
                if (music == null) {
                    music = MediaPlayer.create(this, R.raw.omfghello);
                    music.start();
                    music.setLooping(true);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    this.invalidateOptionsMenu();
                }
                break;
            case R.id.action_stop:
                if (music != null) {
                    music.release();
                    music = null;
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    this.invalidateOptionsMenu();
                }
                break;
        }
        switch (item.getItemId()) {
            case R.id.about_settings:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("About");

                // set dialog message
                alertDialogBuilder
                        .setMessage("SK Telecom T1 is a professional gaming team based in South Korea.\nPreviously, the SK Telecom organization sponsored two sister teams, S and K.\nApp by:\nDiego Roa Centeno\nLyam Jones\nJun-Wei Chim\nSven Verhoeff\nNiek Butijn")
                        .setCancelable(false)
                        .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current dialog
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                return true;
        }
        switch (item.getItemId()) {
            case R.id.about_rules:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("Rules");

                // set dialog message
                alertDialogBuilder
                        .setMessage("This is a simple draw and discard game, suitable for players of all ages. Players have a three card hand and the aim is to collect cards in a single suit worth 31 points or as near as possible to that total.\nA standard 52 card deck is used. For scoring hands, the ace is worth 11 points, the kings, queens, and jacks are worth 10, and all other cards are worth their pip value.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current dialog
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                return true;
        }
        switch (item.getItemId()) {
            case R.id.logout:
                ParseUser.logOut();
                startActivity(new Intent(MenuMain.this, LoginActivity.class));
        }

        switch (item.getItemId()) {
            case R.id.about_users:
                startActivity(new Intent(MenuMain.this, UserList.class));
                break;
        }
        return false;
    }
    public boolean onPrepareOptionsMenu (Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (music == null) {
            mPlayMenuItem.setVisible(true);
            mStopMenuItem.setVisible(false);
        } else if (music != null) {
            mPlayMenuItem.setVisible(false);
            mStopMenuItem.setVisible(true);
        }
        return true;
    }
}
