package com.example.anton.nieuw;

import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anton.Chat;
import com.example.anton.Players;
import com.example.anton.PlayersAdapter;
import com.example.anton.utils.Const;
import com.example.anton.utils.Utils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Player extends Game {
    /**
     * displays result of players ingame
     *
     * @param result = json-string
     */
    protected void gamePlayers(String result) {
        playerList = new ArrayList<Players>();
        adapter = new PlayersAdapter(getApplicationContext(), R.layout.playerrow, playerList);
        list.setAdapter(adapter);

        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonGamePlayersArray = jsonObject.getJSONArray("players");
            String s = "";
            if (jsonGamePlayersArray != null) {
                for (int i = 0; i < jsonGamePlayersArray.length(); i++) {
                    try {
                        Players player = new Players();
                        String jsonGamePlayersString = jsonGamePlayersArray.getString(i);
                        String starter = jsonGamePlayersArray.getString(0);
                        int playerCount = i + 1;
                        if (playerCount == 6) {
                            TextView tView = (TextView) findViewById(R.id.playerFull);
                            String fullRoom = "This room is full!";
                            tView.setText(fullRoom);
                        }
                        playerLimit = playerCount + "/6";
                        if (starter.equals(name)) {
                            Button startgamebutton = (Button) findViewById(R.id.button_startgame);
                            startgamebutton.setVisibility(View.VISIBLE);
                        }
                        player.setPlayer(jsonGamePlayersString);
                        playerList.add(player);
                    } catch (JSONException e) {
                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                    }
                }
            }
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }

    /**
     * Displays player who has token
     *
     * @param result = json-string
     */
    public void gettoken(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            String jsonTokenString = jsonObject.getString("token");
            playerToken = Integer.parseInt(jsonTokenString) - 1;
            Log.v("display", jsonTokenString);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }
    public void WinnerResult(String result) {
        TextView resultWinner = (TextView) findViewById(R.id.resultWinner);
        resultWinner.setText("");
        try {
            JSONObject jsonObject = new JSONObject(result);
            String getWinner = jsonObject.getString("winner");
            resultWinner.setText(getWinner);
        } catch (JSONException e) {
            String noWinner = "Couldn't catch player won";
            resultWinner.setText(noWinner);
        }
    }
}
