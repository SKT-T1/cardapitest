package com.example.anton.nieuw;

import android.graphics.Color;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.anton.Games;
import com.example.anton.GamesAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Diego Roa Centeno on 8-1-2016.
 */
public class Game extends MenuMain {
    protected void initiateGame(String result){
        try {
            JSONObject jsonObject=new JSONObject(result);
            JSONObject jsonInitiateGameObject = jsonObject.getJSONObject("result");
            String gamenr = jsonInitiateGameObject.getString("id");
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(getString(R.string.game_number), gamenr).commit();
            Log.v("display", gamenr);
            //display processed result in textview
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }

    /**
     * displays result of players ingame
     *
     * @param result = json-string
     */

    protected void startingGames(String result) {
        list = (ListView) findViewById(R.id.Listgamesview);
        gamesList = new ArrayList<>();
        GamesAdapter adapter = new GamesAdapter(getApplicationContext(), R.layout.gamesrow, gamesList);
        list.setAdapter(adapter);
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonStartingGamesArray = jsonObject.getJSONArray("games");
            if (jsonStartingGamesArray != null) {
                for (int i = 0; i < jsonStartingGamesArray.length(); i++) {
                    try {
                        Games game = new Games();
                        JSONObject jsonStartingGamesObject = jsonStartingGamesArray.getJSONObject(i);
                        game.setId(jsonStartingGamesObject.getString("id"));
                        game.setPlayer(jsonStartingGamesObject.getString("player"));
                        gamesList.add(game);
                    } catch (JSONException e) {
                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                    }
                }
            }
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < parent.getChildCount(); i++) {
                    parent.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                }
                // change the background color of the selected element
                view.setBackgroundResource(R.drawable.rectangle);
                String gamenr = ((TextView) view.findViewById(R.id.tvId)).getText().toString();
                ((EditText) findViewById(R.id.id_gamenumber)).setText(gamenr);
            }
        });
    }
}
