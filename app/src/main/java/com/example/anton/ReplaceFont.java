package com.example.anton;

import android.content.Context;
import android.graphics.Typeface;
import java.lang.reflect.Field;
/**
 * Created by Niek on 3-12-2015.
 */
public class ReplaceFont {

    public static void replaceDefaultFont (Context context, String nameOffFontBeingReplaced,String nameOffFontInAsset){
        Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(),nameOffFontInAsset);
        replaceFont(nameOffFontBeingReplaced,customFontTypeface);
    }

    private static void replaceFont(String nameOffFontBeingReplaced, Typeface customFontTypeface) {
        try {
            Field myfield = Typeface.class.getDeclaredField(nameOffFontBeingReplaced);
            myfield.setAccessible(true);
            myfield.set(null, customFontTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}