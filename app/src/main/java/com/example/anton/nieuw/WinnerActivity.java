package com.example.anton.nieuw;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.anton.gameplay.CallAPI;
import com.example.anton.gameplay.OnTaskCompleted;

public class WinnerActivity extends Player implements OnTaskCompleted {

    private final OnTaskCompleted onTaskCompleted = this;//enables call-back methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);
        final Button finish = (Button) findViewById(R.id.finish);
        finish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(WinnerActivity.this, ListgamesActivity.class));
            }
        });

        String urlString;
        String name = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Gebruikersnaam), "");
        String gamenr = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.game_number), "");
        urlString = CARD_API + String.format("games/%s/%s/winner",name,gamenr);
        new CallAPI(onTaskCompleted, getString(R.string.resultWinner)).execute(
                urlString,
                getString(R.string.GET));

        //get players
        urlString = CARD_API + String.format("games/%s/%s/players",name,gamenr);
        new CallAPI(onTaskCompleted, getString(R.string.listplayersview)).execute(urlString, getString(R.string.GET));

        if (music != null) {
            music.release();
            music = null;
            music = MediaPlayer.create(this, R.raw.wizkhalifaonmylevel);
            music.start();
            music.setLooping(false);
        }
    }


    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), "You cannot go back.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTaskCompleted(String result, String call) {

        if (call.equals(getString(R.string.listplayersview))) {
            list = (ListView) findViewById(R.id.listplayersviews);
            gamePlayers(result);
        }
        if (call.equals(getString(R.string.resultWinner))) {
            WinnerResult(result);
        }
    }
}


