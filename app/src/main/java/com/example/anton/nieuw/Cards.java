package com.example.anton.nieuw;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Diego Roa Centeno on 8-1-2016.
 */
public class Cards extends Player {
    ArrayList<ImageView> deckCards;
    ArrayList<ImageView> handCards;

    ImageView theDeck1;
    ImageView theDeck2;
    ImageView theDeck3;
    ImageView myCard1;
    ImageView myCard2;
    ImageView myCard3;

    /**
     * processes result of get-deck request
     *
     * @param result = json-string
     */
    protected void getDeck(String result) {
        theDeck1 = (ImageView) findViewById(R.id.deck1);
        theDeck2 = (ImageView) findViewById(R.id.deck2);
        theDeck3 = (ImageView) findViewById(R.id.deck3);
        final int notSelected = getResources().getIdentifier("cardborder", "drawable", getPackageName());
        final int selected = getResources().getIdentifier("cardborderclick", "drawable", getPackageName());
        try {
            JSONObject jsonObject = new JSONObject(result);
            String jsonGetDeckString = jsonObject.getString("cards");
            JSONArray jsonGetDeckArray = new JSONArray(jsonGetDeckString);
            final int deck1 = jsonGetDeckArray.getInt(0);
            final int deck2 = jsonGetDeckArray.getInt(1);
            final int deck3 = jsonGetDeckArray.getInt(2);

            deckCards = new ArrayList<ImageView>();
            deckCards.add(theDeck1);
            deckCards.add(theDeck2);
            deckCards.add(theDeck3);

            String deck01 = "c" + deck1;
            int Deck1 = getResources().getIdentifier(deck01, "drawable", getPackageName());
            for (int i = 0; i <= 3; i++) {

            }
            theDeck1 = (ImageView) findViewById(R.id.deck1);
            theDeck1.setImageResource(Deck1);
            theDeck1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    theDeck1.setBackgroundResource(selected);
                    theDeck2.setBackgroundResource(notSelected);
                    theDeck3.setBackgroundResource(notSelected);
                    String cardOne = deck1 + "";
                    ((EditText) findViewById(R.id.id_cardOne)).setText(cardOne);
                }
            });

            String deck02 = "c" + deck2;
            int Deck2 = getResources().getIdentifier(deck02, "drawable", getPackageName());
            theDeck2 = (ImageView) findViewById(R.id.deck2);
            theDeck2.setImageResource(Deck2);
            theDeck2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    theDeck2.setBackgroundResource(selected);
                    theDeck1.setBackgroundResource(notSelected);
                    theDeck3.setBackgroundResource(notSelected);
                    String cardOne = deck2 + "";
                    ((EditText) findViewById(R.id.id_cardOne)).setText(cardOne);
                }
            });

            String deck03 = "c" + deck3;
            int Deck3 = getResources().getIdentifier(deck03, "drawable", getPackageName());
            theDeck3 = (ImageView) findViewById(R.id.deck3);
            theDeck3.setImageResource(Deck3);
            theDeck3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    theDeck3.setBackgroundResource(selected);
                    theDeck1.setBackgroundResource(notSelected);
                    theDeck2.setBackgroundResource(notSelected);
                    String cardOne = deck3 + "";
                    ((EditText) findViewById(R.id.id_cardOne)).setText(cardOne);
                }
            });
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }

    /**
     * processes result of get-cards request
     *
     * @param result = json-string
     */
    protected void getCards(String result) {
        myCard1 = (ImageView) findViewById(R.id.card1);
        myCard2 = (ImageView) findViewById(R.id.card2);
        myCard3 = (ImageView) findViewById(R.id.card3);
        final int notSelected = getResources().getIdentifier("cardborder", "drawable", getPackageName());
        final int selected = getResources().getIdentifier("cardborderclick", "drawable", getPackageName());
        try {
            JSONObject jsonObject = new JSONObject(result);
            String jsonGetCardsString = jsonObject.getString("cards");
            JSONArray jsonGetCardsArray = new JSONArray(jsonGetCardsString);
            final int card1 = jsonGetCardsArray.getInt(0);
            final int card2 = jsonGetCardsArray.getInt(1);
            final int card3 = jsonGetCardsArray.getInt(2);

            handCards = new ArrayList<ImageView>();
            handCards.add(myCard1);
            handCards.add(myCard2);
            handCards.add(myCard3);

            String card01 = "c" + card1;
            int Card1 = getResources().getIdentifier(card01, "drawable", getPackageName());

            myCard1 = (ImageView) findViewById(R.id.card1);
            myCard1.setImageResource(Card1);
            myCard1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    myCard1.setBackgroundResource(selected);
                    myCard2.setBackgroundResource(notSelected);
                    myCard3.setBackgroundResource(notSelected);
                    String cardTwo = card1 + "";
                    ((EditText) findViewById(R.id.id_cardTwo)).setText(cardTwo);
                }
            });

            String card02 = "c" + card2;
            int Card2 = getResources().getIdentifier(card02, "drawable", getPackageName());
            myCard2 = (ImageView) findViewById(R.id.card2);
            myCard2.setImageResource(Card2);
            myCard2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    myCard2.setBackgroundResource(selected);
                    myCard1.setBackgroundResource(notSelected);
                    myCard3.setBackgroundResource(notSelected);
                    String cardTwo = card2 + "";
                    ((EditText) findViewById(R.id.id_cardTwo)).setText(cardTwo);
                }
            });

            String card03 = "c" + card3;
            int Card3 = getResources().getIdentifier(card03, "drawable", getPackageName());
            myCard3 = (ImageView) findViewById(R.id.card3);
            myCard3.setImageResource(Card3);
            myCard3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    myCard3.setBackgroundResource(selected);
                    myCard1.setBackgroundResource(notSelected);
                    myCard2.setBackgroundResource(notSelected);
                    String cardTwo = card3 + "";
                    ((EditText) findViewById(R.id.id_cardTwo)).setText(cardTwo);
                }
            });
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }

    /**
     * processes result of swap_cards request
     *
     * @param result = json-string
     */
    protected void swapCards(String result) {
        try {
            JSONObject jsonResultStartGameObject = new JSONObject(result);
            String error = jsonResultStartGameObject.getString("error");
            //display error message. Only reached when error part of json-message.
            Toast.makeText(getApplicationContext(), "Error\n" + error, Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }

    /**
     * processes result of swapCard request
     *
     * @param result = json-string
     */
    protected void swapCard(String result) {
        try {
            JSONObject jsonResultStartGameObject = new JSONObject(result);
            String error = jsonResultStartGameObject.getString("error");
            //display error message. Only reached when error part of json-message.
            Toast.makeText(getApplicationContext(), "It's not your turn\n", Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

    }

    /**
     * processes result of pass request
     *
     * @param result = json-string
     */
    protected void pass(String result) {
        try {
            JSONObject jsonResultStartGameObject = new JSONObject(result);
            String error = jsonResultStartGameObject.getString("error");
            //display error message. Only reached when error part of json-message.
            Toast.makeText(getApplicationContext(), "You cannot pass or it's not your turn\n", Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }
    /**
     * displays result of players ingame
     *
     * @param result = json-string
     */
    protected void gamePlayers(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonGamePlayersArray = jsonObject.getJSONArray("players");
            String s = "";
            if (jsonGamePlayersArray != null) {
                for (int i = 0; i < jsonGamePlayersArray.length(); i++) {
                    try {
                        String playerHisTurn = jsonGamePlayersArray.getString(playerToken);
                        playerHasToken = (TextView) findViewById(R.id.playerToken);
                        if (playerHisTurn.length() > 10) {
                            playerHasToken.setTextSize(35);
                        }
                        playerText = playerHisTurn + "\n his turn";
                        playerHasToken.setText(playerText);
                    } catch (JSONException e) {
                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                    }
                }
            }
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }
}
