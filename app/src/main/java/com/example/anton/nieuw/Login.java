package com.example.anton.nieuw;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

public class Login extends Commercial {
    /**
     * displays result of password validation
     *
     * @param result = json-string
     */
    public void validateLogin(String result) {
        String loginResult;
        try {
            JSONObject jsonObject = new JSONObject(result);
            loginResult = jsonObject.getString("result");
            Log.v("Success", loginResult);
            Intent intent = new Intent(Login.this, ListgamesActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } catch (JSONException e) {
            String incorrectPassword = "Password is not correct\n";
            startActivity(new Intent(this, LoginActivity.class));
            Toast.makeText(getApplicationContext(), incorrectPassword, Toast.LENGTH_LONG).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 && resultCode == RESULT_OK)
            finish();

    }
}
