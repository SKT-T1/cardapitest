package com.example.anton.nieuw;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.example.anton.Games;
import com.example.anton.Players;
import com.example.anton.PlayersAdapter;
import com.parse.ParseUser;

import java.util.ArrayList;

public class GlobalVariables extends Activity {
    protected static final String CARD_API = "http://46.105.120.168/cardapi/";
    protected final Context context = this;//to find resources etc
    //persistent data to store user-info, used to identify user
    protected String password;
    protected String name;
    protected String gamenr;
    protected String isInFront;
    protected static MediaPlayer music;
    protected int playerToken;
    protected static TextView playerHasToken;
    protected String playerText;
    protected static String playerLimit;
    protected static ListView list;
    protected static PlayersAdapter adapter;
    protected MenuItem mPlayMenuItem;
    protected MenuItem mStopMenuItem;
    ArrayList<Games> gamesList;
    ArrayList<Players> playerList;
    //check to see if it is on the current activity
    @Override
    public void onResume() {
        super.onResume();
        isInFront = "true";
    }
    //check to see if it is not in the activity
    @Override
    public void onPause() {
        super.onPause();
        isInFront = "false";
    }
}

