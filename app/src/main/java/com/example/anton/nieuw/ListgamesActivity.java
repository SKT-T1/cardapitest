package com.example.anton.nieuw;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anton.gameplay.CallAPI;
import com.example.anton.gameplay.HashMapBuilder;
import com.example.anton.gameplay.OnTaskCompleted;

public class ListgamesActivity extends Game implements OnTaskCompleted {

    private final OnTaskCompleted onTaskCompleted = this;//enables call-back methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listgames);
        //button list games
        final Button listgamebutton = (Button) findViewById(R.id.button_listgames);
        listgamebutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String urlString = CARD_API +String.format("games/%s/starting",name);
                new CallAPI(onTaskCompleted, getString(R.string.startinggames)).execute(urlString, getString(R.string.GET));
                ((EditText) findViewById(R.id.id_gamenumber)).setText("");
            }
        });
        //button initiate game
        final Button initiategamebutton = (Button) findViewById(R.id.button_initiategame);
        initiategamebutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String urlString = CARD_API + String.format("games/%s/initiate", name);
                new CallAPI(onTaskCompleted, getString(R.string.initiate_game)).execute(
                        urlString,
                        getString(R.string.POST),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
            }
        });
        //button_joingame
        final Button joingamebutton = (Button) findViewById(R.id.button_joingame);
        joingamebutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v("display", password);
                String gamenr = ((EditText) findViewById(R.id.id_gamenumber)).getText().toString();
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString(getString(R.string.game_number), gamenr).commit();
                String urlString = CARD_API + String.format("games/%s/apply/%s", name, gamenr);
                if (gamenr.equals("")) {
                    Toast.makeText(getApplicationContext(), "Pick a game from the list.\n", Toast.LENGTH_LONG).show();
                } else {
                    new CallAPI(onTaskCompleted, getString(R.string.join_game)).execute(urlString, getString(R.string.POST),
                            CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                }
            }
        });
        //gets the list games onCreate
        String urlString = CARD_API +String.format("games/%s/starting",name);
        new CallAPI(onTaskCompleted, getString(R.string.startinggames)).execute(urlString, getString(R.string.GET));
        //get saved values for name and password
        name = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Gebruikersnaam), "");
        password = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Wachtwoord), "");
        TextView Name = (TextView) findViewById(R.id.LoginText);
        String welcomeText = "Hello, " + name;
        Name.setText(welcomeText);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onTaskCompleted(String result, String call) {
        if (call.equals(getString(R.string.startinggames))) startingGames(result);else
            if (call.equals(getString(R.string.initiate_game))) {
                initiateGame(result);
                Intent intent = new Intent(ListgamesActivity.this, GameActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }else
            if (call.equals(getString(R.string.join_game))) {
                Intent intent = new Intent(ListgamesActivity.this, GameActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }


    }

}

