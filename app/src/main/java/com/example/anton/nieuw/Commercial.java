package com.example.anton.nieuw;

import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jun-Wei on 6-1-2016.
 */
public class Commercial extends MenuMain {
    /**
     * displays result of commercial
     *
     * @param result = json-string
     */
    public void Commercial(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONObject commercial = jsonObject.getJSONObject("commercial");
            String getCommercialFirstLine = commercial.getString("firstline");
            String getCommercialMessage = commercial.getString("description");
            Toast.makeText(getApplicationContext(), getCommercialFirstLine + "\n" + getCommercialMessage, Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }
}
