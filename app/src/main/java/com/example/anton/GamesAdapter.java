package com.example.anton;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.anton.Games;
import com.example.anton.nieuw.R;


import java.util.ArrayList;

/**
 * Created by Niek on 2-12-2015.
 */
public class GamesAdapter extends ArrayAdapter<Games> {

    ArrayList<Games> ArrayListGames;
    int Resource;
    Context context;
    LayoutInflater vi;

    public GamesAdapter(Context context, int resource, ArrayList<Games> objects) {
        super(context, resource, objects);

        ArrayListGames = objects;
        Resource = resource;
        this.context = context;

        vi = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(Resource, null);
            holder = new ViewHolder();

            holder.tvId = (TextView)convertView.findViewById(R.id.tvId);
            holder.tvPlayer = (TextView)convertView.findViewById(R.id.tvPlayer);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.tvId.setText(ArrayListGames.get(position).getId());
        holder.tvPlayer.setText("Name:" + ArrayListGames.get(position).getPlayer());
        return convertView;

    }

    static class ViewHolder {
        public TextView tvId;
        public TextView tvPlayer;
    }
}