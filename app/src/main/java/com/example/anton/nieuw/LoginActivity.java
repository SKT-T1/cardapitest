package com.example.anton.nieuw;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.anton.UserList;
import com.example.anton.gameplay.CallAPI;
import com.example.anton.gameplay.HashMapBuilder;
import com.example.anton.gameplay.OnTaskCompleted;
import com.example.anton.utils.Utils;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;


public class LoginActivity extends Login implements OnTaskCompleted {
    public final OnTaskCompleted onTaskCompleted = this;//enables call-back methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String urlString;
        setContentView(R.layout.activity_login);

        //Button log in + validation + registration
        final Button Inloggen = (Button) findViewById(R.id.Inloggen);
        Inloggen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //save persistent data in preference manager
                name = ((EditText) findViewById(R.id.etGebruikersnaam)).getText().toString();
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString(getString(R.string.Gebruikersnaam), name).commit();
                password = ((EditText) findViewById(R.id.etWachtwoord)).getText().toString();
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString(getString(R.string.Wachtwoord), password).commit();

                String urlString;
                //Register
                urlString = CARD_API + String.format("players/%s/add/%s", name, name);
                new CallAPI(onTaskCompleted, getString(R.string.LoginResult)).execute(
                        urlString,
                        getString(R.string.POST),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                //validate API call
                urlString = CARD_API + String.format("players/%s/validate", name);
                new CallAPI(onTaskCompleted, getString(R.string.Inloggen)).execute(
                        urlString,
                        getString(R.string.POST),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                //Register and Login Chat
                if (name.length() == 0 || password.length() == 0) {
                    Utils.showDialog(LoginActivity.this, R.string.err_fields_empty);
                    return;
                }
                final ProgressDialog dia = ProgressDialog.show(LoginActivity.this, null,
                        getString(R.string.alert_wait));
                ParseUser.logInInBackground(name, password, new LogInCallback() {

                    @Override
                    public void done(ParseUser pu, ParseException e) {
                        dia.dismiss();
                        if (pu != null) {
                            UserList.user = pu;
                            finish();
                        } else {
                            if (name.length() == 0 || password.length() == 0) {
                                Utils.showDialog(LoginActivity.this, R.string.err_fields_empty);
                                return;
                            }
                            final ProgressDialog dia = ProgressDialog.show(LoginActivity.this, null,
                                    getString(R.string.alert_wait));

                            final ParseUser CreateUser = new ParseUser();
                            CreateUser.setPassword(password);
                            CreateUser.setUsername(name);
                            CreateUser.signUpInBackground(new SignUpCallback() {

                                @Override
                                public void done(ParseException e) {
                                    dia.dismiss();
                                    if (e == null) {
                                        UserList.user = CreateUser;
                                        setResult(RESULT_OK);
                                        finish();
                                    } else {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            e.printStackTrace();
                        }
                    }
                });
            }
        });


        //get saved values,name and password
        name = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Gebruikersnaam), "");
        ((EditText) findViewById(R.id.etGebruikersnaam)).setText(name);
        password = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Wachtwoord), "");
        ((EditText) findViewById(R.id.etWachtwoord)).setText(password);

        //get commercial
        urlString = CARD_API + "commercials/heineken";
        new CallAPI(onTaskCompleted, getString(R.string.Commercial)).execute(
                urlString,
                getString(R.string.GET),
                CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.about_users:
                startActivity(new Intent(this, LoginActivity.class));
                Toast.makeText(getApplicationContext(), "You are not logged in yet.", Toast.LENGTH_LONG).show();
                break;
        }
        switch (item.getItemId()) {
            case R.id.logout:
                Toast.makeText(getApplicationContext(), "You are not logged in yet.", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), "You cannot go back.", Toast.LENGTH_LONG).show();
    }

    //Callback function for CallAPI
    public void onTaskCompleted(String result, String call) {
        if (call.equals(getString(R.string.Commercial))) {
            Commercial(result);
        }
        if (call.equals(getString(R.string.LoginResult))) {
        } else if (call.equals(getString(R.string.Inloggen))) {
            validateLogin(result);
        }
    }
}


