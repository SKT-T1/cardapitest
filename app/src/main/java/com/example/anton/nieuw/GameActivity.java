package com.example.anton.nieuw;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.anton.gameplay.CallAPI;
import com.example.anton.gameplay.HashMapBuilder;
import com.example.anton.gameplay.OnTaskCompleted;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends Player implements OnTaskCompleted {

    private final OnTaskCompleted onTaskCompleted = this;//enables call-back methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        //button startgame
        final Button startgamebutton = (Button) findViewById(R.id.button_startgame);
        startgamebutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                gamenr = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.game_number), "");
                String urlString = CARD_API + String.format("games/%s/%s/start", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.start_game)).execute(urlString, getString(R.string.POST),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
            }
        });
        // task to be done every 2500 milliseconds
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                //get saved values,name, password and gamenr
                name = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Gebruikersnaam), "");
                password = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.Wachtwoord), "");
                gamenr = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.game_number), "");
                String urlString;
                //get players
                urlString = CARD_API + String.format("games/%s/%s/players", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.listplayers)).execute(urlString, getString(R.string.GET));
                //get status
                urlString = CARD_API + String.format("games/%s/%s/state", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.game_status)).execute(urlString, getString(R.string.GET),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
                urlString = CARD_API + String.format("games/%s/%s/token", name, gamenr);
                new CallAPI(onTaskCompleted, getString(R.string.get_token)).execute(urlString, getString(R.string.GET),
                        CallAPI.createQueryStringForParameters(HashMapBuilder.build("password", password)));
            }
        }, 0, 2500);
        //get saved values, gamenr and put it in header
        gamenr = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.game_number), "");
        TextView tView = (TextView) findViewById(R.id.gamenrheader);
        tView.setText(gamenr);
    }
    /**
     * displays result of game status
     *
     * @param result = json-string
     */
    public void gameStatus(String result){
        try {
            JSONObject jsonObject = new JSONObject(result);
            String jsonGameStatusString = jsonObject.getString("status");
            if (jsonGameStatusString.equals("running") && isInFront.equals("true")) {
                Intent intent = new Intent(GameActivity.this, StartGameActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    }

    /**
     * this method is called when get or post-requests are finished
     *
     * @param result = the result of the request, string in JSON-format
     * @param call   = the request-call that has finished
     */
    @Override
    public void onTaskCompleted(String result, String call) {
        if (call.equals(getString(R.string.start_game))) {
            Intent intent = new Intent(GameActivity.this, StartGameActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        if (call.equals(getString(R.string.listplayers))) {
            list = (ListView) findViewById(R.id.listplayersview);
            gamePlayers(result);
            TextView playerAmountText = (TextView) findViewById(R.id.playerCount);
            playerAmountText.setText(playerLimit);
        }
        if (call.equals(getString(R.string.game_status))) {
            gameStatus(result);
        }
    }
}